# jlperona.gitlab.io

Repository for my personal website, hosted with [Jekyll](https://jekyllrb.com/) and [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).
You can access it at [perona.dev](https://perona.dev).

## License

This repository is licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
This includes the images, except where noted.

## Acknowledgments

Thanks to [Aakash Prabhu](http://aakprabhu.com/) for the inspiration behind the website layout.
I have made some changes, but my layout generally follows his.

Also, thanks to Michael Papadopoulous for helping me with setting up the domain and advice on hosting.

## Theme

I use [Minimal Mistakes](https://mmistakes.github.io/minimal-mistakes/), courtesy of [Michael Rose](https://mademistakes.com/).
The website that I originally based mine off of, [Aakash Prabhu's personal website](http://aakprabhu.com/), formerly used [Academic Pages](https://github.com/academicpages/academicpages.github.io) which is a fork of Minimal Mistakes.

## Choice of Host

Originally I was using [GitHub Pages](https://pages.github.com/) to host this website.
I moved to GitLab Pages because of a certain issue with GitHub Pages.
The former problem and possible solutions are listed below.

### Problem: TLS Certificates

I came across an issue in GitHub Pages regarding certificate generation for apex domains versus subdomains.
Before April 2021, GitHub didn't generate TLS certificates for the www subdomain if you wanted to use the apex domain as your main site and have the www subdomain direct to the apex domain.
What this meant was that [_www.perona.dev_](https://www.perona.dev) wouldn't properly redirect to [_perona.dev_](https://perona.dev).
You can read more about this [here](https://github.com/isaacs/github/issues/1675); GitHub [fixed the issue](https://github.blog/changelog/2021-04-08-github-pages-can-now-automatically-secure-the-www-variant-of-your-custom-domain/) in April 2021.

Setting a CNAME record to redirect the subdomain to @ won't work as the certificate served won't be of your domain.
It'll be for GitHub's or GitLab's, which will lead to invalid certificate errors.
This isn't a viable solution.

### Solution 0: Use GitHub Pages (as of April 2021)

GitHub [fixed the issue](https://github.blog/changelog/2021-04-08-github-pages-can-now-automatically-secure-the-www-variant-of-your-custom-domain/) in April 2021.
While that's great, when I initially wrote this it wasn't fixed.
Let's continue on assuming GitHub still has the issue.

### Solution 1: Disable HTTPS

One option to fix this is to disable HTTPS, which is not ideal for security reasons.
Even if you wanted to, you can't do that with the [.dev TLD](https://get.dev/).
.dev websites have [HTTP Strict Transport Security (HSTS)](https://en.wikipedia.org/wiki/HTTP_Strict_Transport_Security) enabled by default.
Websites with HSTS need to be served via HTTPS, so this wouldn't work for me.

### Solution 2: Hacky Workaround

There's a hacky workaround [described here](https://github.community/t/does-github-pages-support-https-for-www-and-subdomains/10360/44) that you could have tried (and see my own attempt in the commit history for this repository).
You'll make GitHub generate the correct certificates, and it'll work for a while.
Unfortunately, not all of them will be auto-renewed, so you'll have to redo the workaround when the certificates expire.
I found this less than ideal.

### Solution 3: Another Certificate Authority

Another solution would have been to serve a certificate for the subdomain via another certificate authority, as suggested [here](https://github.community/t/does-github-pages-support-https-for-www-and-subdomains/10360/7).
This isn't the worst option, but I thought it annoying that I had to consider doing this at all.

### Solution 4: Move to Another Provider

The last solution was to move off GitHub Pages entirely.
I have to thank the author of [this post](https://github.community/t/does-github-pages-support-https-for-www-and-subdomains/10360/54) for mentioning GitLab Pages.

There is one issue related to SEO that moving here has caused, though.
GitLab Pages serves any custom domains ([_www.perona.dev_](https://www.perona.dev) and [_perona.dev_](https://perona.dev)) and the base gitlab.io address ([_jlperona.gitlab.io_](https://jlperona.gitlab.io)) as separate websites, not redirects to one website.
This can have adverse impacts on SEO.
You can read more about this [here](https://gitlab.com/gitlab-org/gitlab/-/issues/14243).

I think migrating here was the best option overall.
Having access to more Ruby gems (instead of just the [whitelisted ones](https://pages.github.com/versions/)) is nice.
Learning how to configure CI was a bonus.
