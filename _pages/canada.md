---
title: Canada
permalink: /travel/canada
layout: splash
excerpt: A gallery of some of the places I've traveled to in Canada.
header:
  overlay_image: /assets/images/banners/canada.jpg
  caption: "Photo credit: **Justin Perona**"
  image_description: Banner image of North Vancouver, British Columbia.
intro:
  - excerpt: >-
      All photos © Justin Perona and licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
      Feel free to [contact me](/contact/) if you'd like originals of any of the photos.
canada:
  - image_path: /assets/images/travel/canada/stanley-park.jpg
    alt: A photo taken in Stanley Park, Vancouver, British Columbia.
    title: Stanley Park, Vancouver, British Columbia
  - image_path: /assets/images/travel/canada/vandusen-botanical-garden.jpg
    alt: A photo taken in VanDusen Botanical Garden, Vancouver, British Columbia.
    title: VanDusen Botanical Garden, Vancouver, British Columbia
---

{% include feature_row id="intro" type="center" %}

{% include feature_row id="canada" %}
