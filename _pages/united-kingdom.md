---
title: United Kingdom
permalink: /travel/united-kingdom
layout: splash
excerpt: A gallery of some of the places I've traveled to in the United Kingdom.
header:
  overlay_image: /assets/images/banners/united-kingdom.jpg
  caption: "Photo credit: **Justin Perona**"
  image_description: Banner image of Crater Lake, Oregon, taken from an airplane.
intro:
  - excerpt: >-
      All photos © Justin Perona and licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
      Feel free to [contact me](/contact/) if you'd like originals of any of the photos.
united_kingdom:
  - image_path: /assets/images/travel/united-kingdom/balmaha.jpg
    alt: A photo taken in Balmaha, Loch Lomond, Scotland.
    title: Balmaha, Loch Lomond, Scotland
  - image_path: /assets/images/travel/united-kingdom/edinburgh.jpg
    alt: A photo taken in Edinburgh, Scotland.
    title: Edinburgh, Scotland
  - image_path: /assets/images/travel/united-kingdom/inchcailloch.jpg
    alt: A photo taken in Inchcailloch, Loch Lomond, Scotland.
    title: Inchcailloch, Loch Lomond, Scotland
---

{% include feature_row id="intro" type="center" %}

{% include feature_row id="united_kingdom" %}
