---
title: Teaching
permalink: /teaching/
header:
  overlay_image: /assets/images/banners/teaching.jpg
  caption: "Photo credit: **Justin Perona**"
  image_description: Banner image of my SIGCSE 2019 badge, academic hood, and graduation stole.
toc: true
links:
  creative-commons: https://creativecommons.org/licenses/
  mage-program: https://sites.google.com/a/mtholyoke.edu/mage/
  mount-holyoke: https://www.mtholyoke.edu/
courses:
  fall-2017: https://github.com/jlpteaching/ECS201A
  fall-2018: https://github.com/jlperona-teaching/ecs390-fall18
  fall-2019: https://github.com/jlperona-teaching/ecs390-fall19
  spring-2015: http://american.cs.ucdavis.edu/academic/ecs154b.s15/
  spring-2016: http://american.cs.ucdavis.edu/academic/ecs154b.s16/
  spring-2017: http://american.cs.ucdavis.edu/academic/ecs154b.s17/
  spring-2018: http://american.cs.ucdavis.edu/academic/ecs154b.s18/
  spring-2019: https://github.com/jlperona-teaching/ecs390-spring19
  spring-2020: https://github.com/jlperona-teaching/ecs390-spring20
  summer-2018: https://github.com/jlperona-teaching/ecs154a-ssii18
  winter-2016: http://american.cs.ucdavis.edu/academic/ecs154b.w16/
  winter-2017: http://american.cs.ucdavis.edu/academic/ecs154b.w17/
  winter-2018: https://github.com/jlpteaching/ECS154B/releases/tag/wq18-end
  winter-2019: https://github.com/jlpteaching/ECS154B/releases/tag/wq19
  winter-2020: https://github.com/jlperona-teaching/ecs154a-winter20/
---

I taught computer science for six years (2015 - 2020) in [UC Davis's Department of Computer Science]({{ site.contact.cs-ucdavis }}).
All the courses I've taught are below; each link leads to the course website for that offering of the course.

For my non-academic projects, see the [Projects tab](/projects/).

## Instructor

Courses that I've taught as instructor of record.
I believe in the open source movement, and so all of my class materials are available with [Creative Commons licenses]({{ page.links.creative-commons }}) on GitHub under my [jlperona-teaching organization]({{ site.contact.github-teaching }}).

### Computer Science Teaching Pedagogy Seminar

Alternative title: "teaching TAs to teach."
A seminar focused on preparing potential teaching assistants to teach computer science at the undergraduate level.
In later iterations of the course, I incorporated material from the [*Megas and Gigas Educate (MaGE) Program*]({{ page.links.mage-program }}) from [Mount Holyoke College]({{ page.links.mount-holyoke }}).
The program is designed to promote inclusive peer mentoring and active learning in computer science.

- [Fall 2018]({{ page.courses.fall-2018 }})
- [Spring 2019]({{ page.courses.spring-2019 }})
- [Fall 2019]({{ page.courses.fall-2019 }})
- [Spring 2020]({{ page.courses.spring-2020 }})

### Computer Architecture I

First undergraduate-level course on computer architecture, focusing on digital design and architectural building blocks like the memory hierarchy.
My courses also included the computer architecture "rite of passage" by having students design a CPU in a circuit simulator.

- [Summer 2018]({{ page.courses.summer-2018 }})
- [Winter 2020]({{ page.courses.winter-2020 }})

## Teaching Assistant

Courses that I've taught as a teaching assistant, including as an undergraduate.

### Computer Architecture II

Second undergraduate-level course on computer architecture, focusing on higher-level computer organization and instruction set architectures (ISAs).

- [Spring 2015]({{ page.courses.spring-2015 }})
- [Winter 2016]({{ page.courses.winter-2016 }})
- [Spring 2016]({{ page.courses.spring-2016 }})
- [Winter 2017]({{ page.courses.winter-2017 }})
- [Spring 2017]({{ page.courses.spring-2017 }})
- [Winter 2018]({{ page.courses.winter-2018 }})
- [Spring 2018]({{ page.courses.spring-2018 }})
- [Winter 2019]({{ page.courses.winter-2019 }})

### Advanced Computer Architecture

Graduate-level course on computer architecture, focusing on advanced concepts such as register allocation, speculation, and parallelism.

- [Fall 2017]({{ page.courses.fall-2017 }})
