---
title: California
permalink: /travel/california
layout: splash
excerpt: A gallery of some of the places I've traveled to in California.
header:
  overlay_image: /assets/images/banners/california.jpg
  caption: "Photo credit: **Justin Perona**"
  image_description: Banner image of Coyote Hills Regional Park, Fremont, California.
intro:
  - excerpt: >-
      All photos © Justin Perona and licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
      Feel free to [contact me](/contact/) if you'd like originals of any of the photos.
california:
  - image_path: /assets/images/travel/california/bodega-bay.jpg
    alt: A photo taken in Bodega Bay, California.
    title: Bodega Bay, California
  - image_path: /assets/images/travel/california/davis.jpg
    alt: A photo taken in Davis, California.
    title: Davis, California
  - image_path: /assets/images/travel/california/death-valley.jpg
    alt: A photo taken in Death Valley, California.
    title: Death Valley, California
  - image_path: /assets/images/travel/california/fremont.jpg
    alt: A photo taken in Fremont, California.
    title: Fremont, California
  - image_path: /assets/images/travel/california/mill-valley.jpg
    alt: A photo taken in Mill Valley, California.
    title: Mill Valley, California
  - image_path: /assets/images/travel/california/napa.jpg
    alt: A photo taken in Napa, California.
    title: Napa, California
  - image_path: /assets/images/travel/california/sacramento.jpg
    alt: A photo taken in Sacramento, California.
    title: Sacramento, California
  - image_path: /assets/images/travel/california/san-diego.jpg
    alt: A photo taken in San Diego, California.
    title: San Diego, California
  - image_path: /assets/images/travel/california/smartsville.jpg
    alt: A photo taken in Smartsville, California.
    title: Smartsville, California
---

{% include feature_row id="intro" type="center" %}

{% include feature_row id="california" %}
