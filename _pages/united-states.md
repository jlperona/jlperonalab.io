---
title: United States
permalink: /travel/united-states
layout: splash
excerpt: >-
  A gallery of some of the places I've traveled to in the United States.
  For California specifically, see the California page.
header:
  overlay_image: /assets/images/banners/united-states.jpg
  caption: "Photo credit: **Justin Perona**"
  image_description: Banner image of Crater Lake, Oregon, taken from an airplane.
intro:
  - excerpt: >-
      All photos © Justin Perona and licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
      Feel free to [contact me](/contact/) if you'd like originals of any of the photos.
united_states:
  - image_path: /assets/images/travel/united-states/tempe.jpg
    alt: A photo taken in Tempe, Arizona.
    title: Tempe, Arizona
  - image_path: /assets/images/travel/united-states/new-orleans.jpg
    alt: A photo taken in New Orleans, Louisiana.
    title: New Orleans, Louisiana
  - image_path: /assets/images/travel/united-states/minneapolis.jpg
    alt: A photo taken in Minneapolis, Minnesota.
    title: Minneapolis, Minnesota
  - image_path: /assets/images/travel/united-states/new-york-city.jpg
    alt: A photo taken in New York City, New York.
    title: New York City, New York
  - image_path: /assets/images/travel/united-states/cannon-beach.jpg
    alt: A photo taken in Cannon Beach, Oregon.
    title: Cannon Beach, Oregon
  - image_path: /assets/images/travel/united-states/gleneden-beach.jpg
    alt: A photo taken in Gleneden Beach, Oregon.
    title: Gleneden Beach, Oregon
  - image_path: /assets/images/travel/united-states/manzanita.jpg
    alt: A photo taken in Manzanita, Oregon.
    title: Manzanita, Oregon
  - image_path: /assets/images/travel/united-states/pacific-city.jpg
    alt: A photo taken in Pacific City, Oregon.
    title: Pacific City, Oregon
  - image_path: /assets/images/travel/united-states/seattle.jpg
    alt: A photo taken in Seattle, Washington.
    title: Seattle, Washington
---

{% include feature_row id="intro" type="center" %}

{% include feature_row id="united_states" %}
