---
title: Contact
permalink: /contact/
header:
  overlay_image: /assets/images/banners/contact.jpg
  caption: "Photo credit: **Justin Perona**"
  image_description: Banner photo of the Hudson River, looking towards North Bergen, New Jersey.
images:
  contact: /assets/images/contact/contact-me.png
---

I can be reached at the following address:

![The address is 'contact at [this website's address]'.]({{ page.images.contact }})

Alternatively, feel free to [reach out to me on LinkedIn]({{ site.contact.linkedin }}) if you prefer!
