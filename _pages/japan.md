---
title: Japan
permalink: /travel/japan
layout: splash
excerpt: A gallery of some of the places I've traveled to in Japan.
header:
  overlay_image: /assets/images/banners/japan.jpg
  caption: "Photo credit: **Justin Perona**"
  image_description: Banner image of the Tempozoan Ferris Wheel in Ōsaka, Japan.
intro:
  - excerpt: >-
      All photos © Justin Perona and licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
      Feel free to [contact me](/contact/) if you'd like originals of any of the photos.
japan:
  - image_path: /assets/images/travel/japan/akihabara.jpg
    alt: A photo taken in Akihabara, Tōkyō.
    title: Akihabara, Tōkyō
  - image_path: /assets/images/travel/japan/ginza.jpg
    alt: A photo taken in Ginza, Tōkyō.
    title: Ginza, Tōkyō
  - image_path: /assets/images/travel/japan/imperial-palace.jpg
    alt: A photo taken at the Imperial Palace, Tōkyō.
    title: Imperial Palace, Tōkyō
  - image_path: /assets/images/travel/japan/meiji-shrine.jpg
    alt: A photo taken at the Meiji Shrine, Tōkyō.
    title: Meiji Shrine, Tōkyō
  - image_path: /assets/images/travel/japan/mitaka.jpg
    alt: A photo taken in Mitaka, Tōkyō Prefecture.
    title: Mitaka, Tōkyō Prefecture
  - image_path: /assets/images/travel/japan/shibuya.jpg
    alt: A photo taken in Shibuya, Tōkyō.
    title: Shibuya, Tōkyō
  - image_path: /assets/images/travel/japan/namba.jpg
    alt: A photo taken in Namba, Ōsaka.
    title: Namba, Ōsaka
  - image_path: /assets/images/travel/japan/shinsekai.jpg
    alt: A photo taken in Shinsekai, Ōsaka.
    title: Shinsekai, Ōsaka
  - image_path: /assets/images/travel/japan/wakayama.jpg
    alt: A photo taken in Wakayama, Wakayama Prefecture.
    title: Wakayama, Wakayama Prefecture
---

{% include feature_row id="intro" type="center" %}

{% include feature_row id="japan" %}
