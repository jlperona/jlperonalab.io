---
title: Home
permalink: /
header:
  overlay_image: /assets/images/banners/home.jpg
  caption: "Photo credit: **Sarah Fisher**"
  image_description: >-
    Banner image of me looking towards Pacific City, Oregon.
    Photo taken at the top of Cape Kiwanda Sand Dune in Cape Kiwanda State Natural Area, Oregon.
images:
  bunny-suit: /assets/images/home/bunny-suit.jpg
---

Hi!
I am a computer scientist living in the Portland metro area, currently working as an Infrastructure and DevOps Engineer at Intel Corporation.
I received my Master's degree in Computer Science (2019) and my Bachelor's degree in Computer Science & Engineering (2016), both from [UC Davis]({{ site.contact.cs-ucdavis }}).

During my time at Davis, I taught computer architecture and computer science teaching pedagogy classes.
I was also an undergraduate and graduate peer adviser, advising university students of all types.
Significant portions of my work for both roles are open-source and [available on GitHub]({{ site.contact.github }}).

Feel free to [contact me](/contact/) if you want to chat.
Cheers!
