---
title: Travel
permalink: /travel/
layout: splash
excerpt: A gallery of some of the places I've traveled to over the years.
header:
  overlay_image: /assets/images/banners/travel.jpg
  caption: "Photo credit: **Justin Perona**"
  image_description: Banner image of the San Francisco East Bay, taken from an airplane.
intro:
  - excerpt: >-
      Click on the images below to go to the gallery for that region.
regions:
  - image_path: /assets/images/travel/california.jpg
    alt: >-
      Photo of Coyote Hills Regional Park, Fremont, California.
      Click on this image to go to the gallery for California.
    url: /travel/california
  - image_path: /assets/images/travel/canada.jpg
    alt: >-
      Photo of Sunset Beach in Vancouver, British Columbia.
      Click on this image to go to the gallery for Canada.
    url: /travel/canada
  - image_path: /assets/images/travel/japan.jpg
    alt: >-
      Photo of the Tempozoan Ferris Wheel in Ōsaka, Japan.
      Click on this image to go to the gallery for Japan.
    url: /travel/japan
  - image_path: /assets/images/travel/united-kingdom.jpg
    alt: >-
      Photo of Duddingston and Duddingston Loch in Edinburgh, Scotland.
      Click on this image to go to the gallery for the United Kingdom.
    url: /travel/united-kingdom
  - image_path: /assets/images/travel/united-states.jpg
    alt: >-
      Photo of Crater Lake, Oregon, taken from an airplane.
      Click on this image to go to the gallery for the United States.
    url: /travel/united-states
---

{% include feature_row id="intro" type="center" %}

<!-- To create banners for this page, resize the image to 600x450. -->
<!-- Place a background layer of #808080, then make the opacity of the layer 50%. -->
<!-- The text should be Roboto Medium, 84 point, #FFFFFF, and centered in the image. -->

{% include gallery id="regions" %}
