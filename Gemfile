# frozen_string_literal: true

source 'https://rubygems.org'

gem 'jekyll', '~> 4.3'

group :jekyll_plugins do
  # theme gem
  gem 'minimal-mistakes-jekyll', '~> 4'

  # theme gem dependencies
  gem 'jekyll-include-cache'

  # add gems to compress instead of doing it via CI
  gem 'jekyll-brotli'
  gem 'jekyll-gzip'

  # recommended for localization files with Minimal Mistakes
  # https://mmistakes.github.io/minimal-mistakes/docs/quick-start-guide/#migrating-to-gem-version
  gem 'jekyll-data'

  # add last updated dates automatically
  gem 'jekyll-last-modified-at'

  # redirect for one specific page
  gem 'jekyll-redirect-from'
end

group :development do
  gem 'rubocop', require: false
end
