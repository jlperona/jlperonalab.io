---
title: perona.dev
excerpt: The website you're currently viewing. Built with Ruby, Jekyll, and Minimal Mistakes.
header:
  teaser-full: /assets/images/projects/perona-dev/teaser-full.jpg
  teaser: /assets/images/projects/perona-dev/teaser.jpg
images:
  responsiveness: /assets/images/projects/perona-dev/responsiveness.jpg
links:
  aakash: http://aakprabhu.com/
  aakash-repo: https://github.com/aakash1104/aakash1104.github.io
  eggs: https://nextcloud.com/blog/dont-mess-with-the-google-the-risk-of-putting-all-your-eggs-in-one-basket/
  gatsby: https://www.gatsbyjs.com/
  gatsby-test: https://github.com/jlperona/jekyll-to-gatsby-test
  git: https://git-scm.com/
  github: https://github.com/
  github-archival: https://github.com/jlperona/jlperona-github-io-archival
  github-pages: https://pages.github.com/
  github-tls-blog: https://github.blog/changelog/2021-04-08-github-pages-can-now-automatically-secure-the-www-variant-of-your-custom-domain/
  github-tls-discussion: https://github.community/t/does-github-pages-support-https-for-www-and-subdomains/10360
  github-tls-issue: https://github.com/isaacs/github/issues/1675
  github-tls-workarounds: https://gitlab.com/jlperona/jlperona.gitlab.io#choice-of-host
  gitlab-pages: https://docs.gitlab.com/ee/user/project/pages/
  gitlab-repo: https://gitlab.com/jlperona/jlperona.gitlab.io
  gitlab-repo-ci: https://gitlab.com/jlperona/jlperona.gitlab.io/-/blob/main/.gitlab-ci.yml
  hover: https://www.hover.com/
  jekyll: https://jekyllrb.com/
  michael-rose: https://mademistakes.com/
  minimal-mistakes: https://mmistakes.github.io/minimal-mistakes/
  na-tld: https://en.wikipedia.org/wiki/.na
  node-js: https://nodejs.org/en/
  protonmail: https://protonmail.com/
  ruby: https://www.ruby-lang.org/en/
  tls: https://en.wikipedia.org/wiki/Transport_Layer_Security
  wakayama-castle: https://en.wikipedia.org/wiki/Wakayama_Castle
---

It took me six months (October 2020 - April 2021) of sporadic work to get this website where I wanted it to be.
This page details how this website was developed, my thoughts, and what's next.

![The teaser image and main page's banner image, me looking towards Coyote Hills Regional Park in Fremont, California.]({{ page.header.teaser-full }})

## Past: Retrospective

Alternatively: how I went from "knowing nothing about owning a domain" to Jekyll competency in six months.

### Professional Email First

When I first started this process in October 2020, building a personal website wasn't a priority for me.
I mostly wanted a professional email address to replace `unprofessional.email at gmail dot com` and `last.name at former.university dot edu`.
Also, I wanted to [stop putting all my eggs in one basket]({{ page.links.eggs }}) and move my email accounts away from Google.
So I did my research and:

- found a reasonable domain name ([{{ page.title }}](/))
  - `pero.na` uses the [.na TLD]({{ page.links.na-tld }}) which is incredibly expensive
  - `perona.com` has been taken since at least the early 2000s
- found a good domain name registrar ([Hover]({{ page.links.hover }}))
- found a good email host ([ProtonMail]({{ page.links.protonmail }}))
- paid for everything, set up the DNS records, and forwarded my previous email addresses to the new one

And with that, I had my professional email address hosted somewhere other than Google.

### Imitating with Jekyll and GitHub Pages

Once the email was set up, I thought "if it's easy to make a website I'll consider doing it."
It was my friend, [Aakash Prabhu]({{ page.links.aakash }}), and [his personal website]({{ page.links.aakash-repo }}) hosted via [GitHub Pages]({{ page.links.github-pages }}), that made me think about how easy it could be.
I was already used to [Git]({{ page.links.git }}) and [GitHub]({{ page.links.github }}), and GitHub Pages looked interesting enough.
I didn't know anything about [Jekyll]({{ page.links.jekyll }}), [Ruby]({{ page.links.ruby }}), or web development, but who cares?
Imitate until you begin to understand.

With that mindset, I imitated his setup: Jekyll as the static site generator (SSG) and GitHub Pages as the host.
Some DNS records and a couple of commits later, I got my "hello world" page set up.
Three months, many more commits, and a lot more imitation later, the website began to take shape.

### From GitHub to GitLab

As I began to learn more about web development, one particular issue with GitHub Pages irked me.
At the time, GitHub didn't generate [Transport Layer Security (TLS)]({{ page.links.tls }}) certificates for the www subdomain if you wanted to use an apex domain as your main site and have the www subdomain direct to the apex domain.
In other words, _www.{{ page.title }}_ wouldn't properly redirect to _{{ page.title }}_.
This problem with GitHub Pages was well-known; an [issue on GitHub]({{ page.links.github-tls-issue }}) was opened October 2019, and [other discussions]({{ page.links.github-tls-discussion }}) active since May 2018.
GitHub [finally fixed it]({{ page.links.github-tls-blog }}) in April 2021.

Before GitHub fixed it, there were [a few workarounds]({{ page.links.github-tls-workarounds }}), but I decided that the best option was to move to [GitLab Pages]({{ page.links.gitlab-pages }}) as my hosting provider.
I had to learn how to write [my own continuous integration (CI) script]({{ page.links.gitlab-repo-ci }}), but the benefits were worth doing so.
GitLab Pages doesn't have these certificate issues, and I learned how to configure CI as a bonus.

### Appropriate Image Sizes

Implementing appropriate image sizes was the last major change I made.
When I was originally generating images to use on pages, I read something that said "stick to 1280px width for images."
Don't make your images too large, otherwise page performance suffers.
It made sense, so every image I used became 1280px wide.
This wasn't bad advice, but even 1280px can be too big.

Loading certain pages of my website on my phone alerted me to the problem.
Eventually, I figured out that you shouldn't size your images above what they're being used for.
Do you have splash images, like those on the main tabs of this website?
Make those 1920px width, since they're going to be used at that resolution.
On the other hand, images like those on the [Travel tab](/travel) that are never sized above 600px width?
1280px width there is _incredibly_ wasteful, there's no benefit whatsoever.

Originally, that page had 25 images totaling ~8 MB.
I resized all the images (besides the splash image) to 600px width.
This cut down the total image size to ~1.2 MB—almost a 7x reduction in size!
The image below shows an example of that reduction with an image of [Wakayama Castle]({{ page.links.wakayama-castle }}).
The original image is at the top, and the resized one below it.
Compare the file sizes and visual fidelity at the width _both_ images would have been used at.

![An example of reducing the size of the image of Wakayama Castle on the Travel tab. 509 KB at 1280px width was reduced to 69.5 KB at 600px width with little difference in visual quality.]({{ page.images.responsiveness }})

I repeated this process across all the images on the website.
Reducing image sizes shaved multiple tenths of seconds (if not whole seconds) from page load times.

## Present: A Lesson Learned

The biggest lesson I learned:

> Don't jump into the deep end of web development if you're new to it.

Sounds obvious, but it's worth mentioning.
If I had started with a more complicated SSG, I probably would have quit before getting to this point.
Hell, if I had started with Jekyll on GitLab Pages I might have quit before this too.
Jekyll on GitHub Pages was beginner-friendly enough that I was able to pick up the basics quickly.
Once I got those basics down, then I could start using some of the advanced features and consider moving the hosting.

## Future: Next Steps

What's next for this website?
I'll continue updating content and adding projects as I see fit, but I don't have any big changes planned.
Unless I find a new theme or start customizing [the current theme]({{ page.links.minimal-mistakes }}) more heavily, things are looking stable at the moment.

That said, I've been toying around with [Gatsby]({{ page.links.gatsby }}), a different static site generator built on [Node.js]({{ page.links.node-js }}).
In my [experiments in moving this website over to Gatsby]({{ page.links.gatsby-test }}), Gatsby comes off as incredibly responsive, especially with images.
Unfortunately, I don't know enough JS and CSS yet to implement all the things I want to do, so it'll remain an experiment for now until I learn more.

## Acknowledgments

Thanks to the following people that helped make this website a reality:

- [Aakash Prabhu]({{ page.links.aakash }}), whose personal website was the inspiration for mine
- Michael Papadoupolous, for sanity-checking my plans before I started
- [Michael Rose]({{ page.links.michael-rose }}), for creating the theme that this website uses, [Minimal Mistakes]({{ page.links.minimal-mistakes }})
