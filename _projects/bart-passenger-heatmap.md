---
title: BART Passenger Heatmap
excerpt: Shiny app for displaying BART hourly origin-destination data.
header:
  teaser-full: /assets/images/projects/bart-passenger-heatmap/teaser-full.jpg
  teaser: /assets/images/projects/bart-passenger-heatmap/teaser.jpg
gallery:
  - url: /assets/images/projects/bart-passenger-heatmap/gallery-1.jpg
    image_path: /assets/images/projects/bart-passenger-heatmap/gallery-1-th.jpg
    alt: >-
      One of the San Francisco Bay Area Rapid Transit (BART) trains.
      I used BART's passenger data as the basis for my heatmap.
    title: >-
      One of the San Francisco Bay Area Rapid Transit (BART) trains.
      I used BART's passenger data as the basis for my heatmap.
  - url: /assets/images/projects/bart-passenger-heatmap/gallery-2.jpg
    image_path: /assets/images/projects/bart-passenger-heatmap/gallery-2-th.jpg
    alt: >-
      A PowerPoint slide detailing my favorite part in designing the heatmap.
      This slide came from my project presentation for ECI 254, the class I made this heatmap for.
    title: >-
      A PowerPoint slide detailing my favorite part in designing the heatmap.
      This slide came from my project presentation for ECI 254, the class I made this heatmap for.
  - url: /assets/images/projects/bart-passenger-heatmap/gallery-3.jpg
    image_path: /assets/images/projects/bart-passenger-heatmap/gallery-3-th.jpg
    alt: >-
      The default view for the heatmap, visually showing the distribution of passengers on the BART network.
    title: >-
      The default view for the heatmap, visually showing the distribution of passengers on the BART network.
links:
  bart: https://www.bart.gov/
  bart-parser: https://github.com/jlperona/bart-hourly-dataset-parser
  bart-soo: https://www.bart.gov/about/reports/ridership
  dsouza: http://mae.engr.ucdavis.edu/dsouza/
  github-preparser: https://github.com/jlperona/bart-passenger-heatmap/tree/master/preparser
  github-repo: https://github.com/jlperona/bart-passenger-heatmap
  github-rmd: https://github.com/jlperona/bart-passenger-heatmap/blob/master/bart-passenger-heatmap.Rmd
  leaflet: https://rstudio.github.io/leaflet/
  mit-license: https://opensource.org/licenses/MIT
  niemeier: https://faculty.engineering.ucdavis.edu/dniemeier/
  r: https://www.r-project.org/
  rstudio: https://www.rstudio.com/
  shiny: https://shiny.rstudio.com/
---

You can run this project in [RStudio]({{ page.links.rstudio }}) with the following lines:

```
library(shiny)
shiny::runGitHub("bart-passenger-heatmap", "jlperona")
```

![The teaser image, which is a picture of a San Francisco Bay Area Rapid Transit (BART) train.]({{ page.header.teaser-full }})

## Background

During the course of my Master's degree, I took ECI 254 (Exploring Data from Built Environment Using R) with [Professor Deb Niemeier]({{ page.links.niemeier }}).
My choice for the final project for that class was to create a heatmap of passengers that traveled through the [San Francisco Bay Area Rapid Transit (BART)]({{ page.links.bart }}) system.
As the class required the use of [R]({{ page.links.r }}), I decided to use [Shiny]({{ page.links.shiny }}) to help me build the heatmap.

The heatmap is heavily based on one of my previous projects, [jlperona/bart-hourly-dataset-parser]({{ page.links.bart-parser }}).
I wrote that parser for MAE 253 (Network Theory and Applications) with [Professor Raissa D'Souza]({{ page.links.dsouza }}).
Its purpose is to parse [BART hourly origin-destination data]({{ page.links.bart-soo }}) into network files that can be imported into another program.

## Project

I wanted to see the BART network mapped with actual GIS data, and with the passenger data that BART provides on their website.
One of the ways of doing that was by combining Shiny, which helps me create a web application, with [*leaflet*]({{ page.links.leaflet }}), which lets me map the GIS data.

### Methodology

To do so, I wrote a [preparser]({{ page.links.github-preparser }}) to first parse all the data into files that could be quickly parsed by the application.
After I had the files, I wrote the code to parse the data in plain R, then the code to render the data as a heatmap using Shiny and _leaflet_.
For more details on the technical aspects of the project, see [this RMarkdown document in the repository]({{ page.links.github-rmd }}).

### Source Code and Installation

All code is open-source and available on GitHub at [jlperona/bart-passenger-heatmap]({{ page.links.github-repo }}).
Both it and its predecessor are licensed under the [MIT License]({{ page.links.mit-license }}).

You can run this project in [RStudio]({{ page.links.rstudio }}) with the following lines:

```
library(shiny)
shiny::runGitHub("bart-passenger-heatmap", "jlperona")
```

## Gallery

Gallery of relevant photos to this project.

{% include gallery %}
