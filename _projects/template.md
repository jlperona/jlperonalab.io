---
title: Template
excerpt: Template for project posts.
header:
  teaser-full: /assets/images/site/masthead.png # 800x600, suffix with -full
  teaser: /assets/images/site/masthead.png # 600x450
gallery:
  - url: /assets/images/site/masthead.png # 1280x960 or other consistent height
    image_path: /assets/images/site/masthead.png # 600x450 or other consistent height, suffix with -th
    alt: Example gallery post.
    title: Example gallery post.
published: false
---

_Under construction._

![The teaser image, under construction.]({{ page.header.teaser-full }})

## Background

_Under construction._

## Project

_Under construction._

## Gallery

Gallery of relevant photos to this project.

{% include gallery %}
